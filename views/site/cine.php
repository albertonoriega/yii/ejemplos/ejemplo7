<?php
// ponemos el titulo a la pestaña del navegador
$this->title = "Noticias de cine";

// Ponemos un titulo a la pagina
?>
<h1> Noticias de cine</h1>
<br>
<br>

<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider'=> $dataProvider,
    'columns'=> [
        ['attribute' => 'title',
        'label' => 'Título',
        ],
        [
            'label' => 'Enlace',
            'content' => function ($dato) {
                return Html::a(
                        "Ir a la noticia", 
                        $dato->link,
                        ["class" => "btn btn-light text-danger m-3"]
                );
            }
        ],
        [
            'attribute' => 'description',
            'label' => ' Descripción',
            'content' => function ($dato){
                
                $contenido = str_replace(
                        "src", // busco la palabra src
                        //"width= 200 src", // cambio src por este texto (asi logramos hacer la imagen más pequeña)
                        'class= "img-thumbnail" src', // clase de bootstrap que hace que la foto se vaya reduciendo a la vez que aumenta la resolucion
                        $dato->description); // Texto donde va a buscar y cambiar el contenido
                return $contenido;
                
            }
        ],
        
        
    ]
]);
