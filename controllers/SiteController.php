<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\LoginForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use const YII_ENV_TEST;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // Dirección del canal rss
        $url="https://www.eldiariomontanes.es/rss/2.0/?section=ultima-hora";
        // Leer una página web
        $contenido = file_get_contents($url);
        //var_dump($contenido);
        // funcion de php que convierte un xml a un objeto
        // en channel->item es donde estan las noticias
        // Esto te daría todas las noticias pero solo te devuelve la primera
        $ObjetoTiponoticias=simplexml_load_string($contenido)->channel->item;
        
        
        // Voy a crear un array con todas las noticias
        foreach ($ObjetoTiponoticias as $noticia) {
            $noticias[]=$noticia;
        }
        // $noticias es un array con todas la noticias
        //var_dump($noticias);
        
        // vamos a mostrar todas las noticias en un GRIDVIEW
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $noticias,
            // Paginacion de 10 en 10 (hay que hacerlo en el Data Provider)
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        //var_dump($noticias);
        return $this->render('index',[
            "dataProvider" => $dataProvider,
            
        ]);
    }

    public function actionRecetas() {

        $url = "https://www.eldiariomontanes.es/rss/2.0/?section=cantabria-mesa/recetas";

        $contenido = file_get_contents($url);

        $ObjetoTiponoticias = simplexml_load_string($contenido)->channel->item;

        foreach ($ObjetoTiponoticias as $noticia) {
            $noticiasRecetas[] = $noticia;
        }

            //var_dump($noticiasRecetas);
            $dataProviderRecetas = new ArrayDataProvider([
                "allModels" => $noticiasRecetas,
            ]);

            return $this->render('recetas', [
                        "dataProvider" => $dataProviderRecetas,
            ]);
        
    }

    public function actionCine() {

        $url = "https://www.eldiariomontanes.es/rss/2.0/?section=culturas/cine";

        $contenido = file_get_contents($url);

        $ObjetoTiponoticias = simplexml_load_string($contenido)->channel->item;

        foreach ($ObjetoTiponoticias as $noticia) {
            $noticiascine[] = $noticia;
        }
            $dataProvider = new ArrayDataProvider([
                "allModels" => $noticiascine,
            ]);

            return $this->render('cine', [
                        "dataProvider" => $dataProvider,
            ]);
        
    }

}
